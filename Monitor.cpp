#include "pin.H"
#include "RTNContainer.h"
#include "IMGContainor.h"
#include "IMGContainorMap.h"
#include <iostream>
#include <fstream>

static ofstream outFile;
static IMGContainor myIMGContainor = IMGContainor();
static IMGContainorMap myIMGCMap = IMGContainorMap();
static ADDRINT firstIMGAddr = 0;
static ADDRINT firstIMGHighAddr = 0;
static string firstIMGName = "";
static string prevIMGName = "";
static string prevRTNName = "";
static ADDRINT prevRTNAddr = 0;
static ADDRINT prevINSAddr = 0;
static bool procedureCheck = false;
static map<ADDRINT, ADDRINT> InsAddrMap;
static list<ADDRINT> insAddrList;
static list<string> imgNameList;
static int rtnCount = 0;
static ADDRINT syscallAddr = 0;

VOID routine(RTN rtn, VOID *v) {

	if (rtnCount == 0) {
		bool rtnValid = RTN_Valid(rtn);

		if (rtnValid) {
			ADDRINT rtnAddress = RTN_Address(rtn);
			IMG rtnIMG = IMG_FindByAddress(rtnAddress);
			bool rtnIMGValid = IMG_Valid(rtnIMG);

			if (rtnIMGValid) {
				ADDRINT rtnIMGAddress = IMG_LowAddress(rtnIMG);
				IMGContainorMap firstIMGCMap = IMGContainorMap();
				firstIMGCMap.inputIMG(rtnIMG);
				firstIMGCMap.inputRTN(rtn);
				myIMGCMap = firstIMGCMap;
				firstIMGAddr = rtnIMGAddress;
				string rtnIMGName = IMG_Name(rtnIMG);
				firstIMGName = rtnIMGName;
				firstIMGHighAddr = IMG_HighAddress(rtnIMG);

				rtnCount++;
			}
		}
	}
	if (rtnCount == 1) {
		auto nameList = myIMGCMap.returnIMGNames();
		for (string name : nameList) {
			outFile << "First IMG(Executable) : " << name << endl <<
				"Low Address : " << hex << firstIMGAddr << endl <<
				"High Address : " << hex << firstIMGHighAddr << endl;
			rtnCount++;
		}
	}

}

VOID trackIns(ADDRINT currAddr) {

	PIN_LockClient();

	RTN currRTN = RTN_FindByAddress(currAddr);
	IMG currIMG = IMG_FindByAddress(currAddr);
	bool currRTNValid = RTN_Valid(currRTN);
	bool currIMGValid = IMG_Valid(currIMG);



	if (currIMGValid && currRTNValid) {
		string currRTNName = RTN_Name(currRTN);
		string currIMGName = IMG_Name(currIMG);

		if (prevIMGName != "" && firstIMGName != "") {
			if (currIMGName == firstIMGName && prevIMGName != firstIMGName) {
				outFile << hex << currAddr << " : " << prevRTNName << " : " << prevIMGName << endl;
			}
			// for bozok
			//for book end
		}
		
		prevIMGName = currIMGName;
		prevRTNName = currRTNName;

	}
	// current address is in undefined section
	else {
		if (prevIMGName != "" && firstIMGName != "" && syscallAddr != 0) {
			if(prevIMGName != firstIMGName){
				if(currAddr != syscallAddr){
				outFile << hex << currAddr << " : " << prevRTNName << " : " << prevIMGName << endl;
				}
			}
		}
		prevIMGName = "";
		prevRTNName = "";
	}

	InsAddrMap[currAddr] = prevINSAddr;
	prevINSAddr = currAddr;
	insAddrList.push_back(currAddr);

	PIN_UnlockClient();
}


VOID trackIns3(INS ins) {
	bool sysCheck = INS_IsSyscall(ins);
	if (sysCheck) {
		ADDRINT sysAdd = INS_Address(ins);
		outFile << " Syscall : " << hex << sysAdd << endl;
	}
}


VOID Trace(TRACE trace, VOID *v) {

	for (BBL bbl = TRACE_BblHead(trace); BBL_Valid(bbl); bbl = BBL_Next(bbl)) {
		for (INS ins = BBL_InsHead(bbl); INS_Valid(ins); ins = INS_Next(ins)) {
			//trackIns3(ins);
			INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)trackIns, IARG_INST_PTR, IARG_END);
		}
	}
}


VOID instruction(INS ins, VOID *v) {
	ADDRINT insAddress = INS_Address(ins);
	if(syscallAddr == 0){
		bool sysCheck = INS_IsSyscall(ins);
		if (sysCheck) {
			syscallAddr = insAddress;
		}
	}

}

VOID Fini(INT32 code, VOID *v) {

	outFile << endl << "Img Names " << endl;
	for (string name : imgNameList) {
		outFile << name << endl;
	}

	outFile << endl << " - Finished - " << endl;
}

INT32 Usage() {

	cerr << "This tool collects APIs" << endl;
	return -1;
}

int main(INT32 argc, char *argv[]) {

	PIN_InitSymbols();

	if (PIN_Init(argc, argv)) {
		return Usage();
	}

	outFile.open("result.txt");

	INS_AddInstrumentFunction(instruction, 0);
	TRACE_AddInstrumentFunction(Trace, 0);
	RTN_AddInstrumentFunction(routine, 0);

	PIN_AddFiniFunction(Fini, 0);
	PIN_StartProgram();

	return 0;
}