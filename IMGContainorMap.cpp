#include "IMGContainorMap.h"

VOID IMGContainorMap::inputIMG(IMG inputIMG)
{
	bool inputIMGValid = IMG_Valid(inputIMG);
	if (inputIMGValid) {
		bool inputIMGContain = checkContainIMG(inputIMG);
		if(inputIMGContain == false){
			IMGContainor newIMGC = IMGContainor(inputIMG);
			ADDRINT newIMGCAddr = newIMGC.returnAddress();
			IMGCMap[newIMGCAddr] = newIMGC;
			trim();
		}
	}
}

VOID IMGContainorMap::inputRTN(RTN inputRTN)
{
	bool inputRTNValid = RTN_Valid(inputRTN);
	if (inputRTNValid) {
		ADDRINT inputRTNAddr = RTN_Address(inputRTN);
		IMG inputRTNIMG = IMG_FindByAddress(inputRTNAddr);
		bool inputRTNIMGValid = IMG_Valid(inputRTNIMG);
		
		if (inputRTNIMGValid) {
			bool containInputRTNIMG = checkContainIMG(inputRTNIMG);

			if (containInputRTNIMG) {
				ADDRINT inputRTNIMGAddr = IMG_LowAddress(inputRTNIMG);
				IMGContainor containgOne = IMGCMap[inputRTNIMGAddr];
				containgOne.inputRTN(inputRTN);
				IMGCMap[inputRTNIMGAddr] = containgOne;
			}
			else {
				IMGContainor newIMGC = IMGContainor(inputRTNIMG);
				bool newIMGCValid = newIMGC.checkValid();
				if (newIMGCValid) {
					ADDRINT newIMGCAddr = newIMGC.returnAddress();
					IMGCMap[newIMGCAddr] = newIMGC;
				}
			}
		}
	}
}

bool IMGContainorMap::checkContainIMG(IMG checkIMG)
{
	bool checkIMGValid = IMG_Valid(checkIMG);
	if (checkIMGValid == false) {
		return false;
	}
	else {
		ADDRINT checkIMGAddr = IMG_LowAddress(checkIMG);
		int countCheckIMGAddr = IMGCMap.count(checkIMGAddr);
		
		if (countCheckIMGAddr == 1) {
			return true;
		}
		else {
			false;
		}
	}
	return false;
}

VOID IMGContainorMap::trim()
{
	int IMGCMapSize = IMGCMap.size();
	if (IMGCMapSize > 1) {
		if (IMGCMap.find(0) != IMGCMap.end()) {
			IMGCMap.erase(0);
		}
	}
}

map<ADDRINT, IMGContainor> IMGContainorMap::returnIMGCMap()
{
	return IMGCMap;
}

int IMGContainorMap::returnIMGNumber()
{
	auto myMap = returnIMGCMap();
	int myMapSize = myMap.size();
	return myMapSize;
}

list<string> IMGContainorMap::returnIMGNames()
{	
	list<string> resultList;
	for (auto it = IMGCMap.begin(); it != IMGCMap.end(); ++it) {
		IMGContainor IMGC = it->second;
		string IMGCName = IMGC.returnName();
		resultList.push_back(IMGCName);
	}

	return resultList;

}
