#include "IMGContainor.h"

bool IMGContainor::checkValid()
{
	return valid;
}

VOID IMGContainor::changeValid()
{
	if (valid) {
		valid = false;
	}
	else {
		valid = true;
	}
}

VOID IMGContainor::inputName(string newName)
{
	name = newName;
}

VOID IMGContainor::inputAddress(ADDRINT newAddress)
{
	address = newAddress;
}

ADDRINT IMGContainor::returnAddress()
{
	return address;
}

string IMGContainor::returnName()
{
	return name;
}

bool IMGContainor::checkContainRTN(RTN checkRTN)
{
	bool valid = RTN_Valid(checkRTN);
	if (valid == false) {
		return false;
	}
	else {
		ADDRINT checkRTNAddr = RTN_Address(checkRTN);
		IMG newIMG = IMG_FindByAddress(checkRTNAddr);
		bool newIMGValid = IMG_Valid(newIMG);
		
		if (newIMGValid == false) {
			return false;
		}
		else {
			string newIMGName = IMG_Name(newIMG);
			if (newIMGName == name) {
				return true;
			}
			else {
				return false;
			}
		}
	}
}

VOID IMGContainor::inputRTN(RTN inputRTN)
{
	ADDRINT inputRTNAddress = RTN_Address(inputRTN);
	bool containRTN = checkContainRTN(inputRTN);
	
	if (containRTN) {
		RTNContainer newRC = RTNContainer(inputRTNAddress);
		RCMap[inputRTNAddress] = newRC;
	}
}

VOID IMGContainor::trim()
{
	int RCMapSize = RCMap.size();
	if (RCMapSize > 1){
		if(RCMap.find(0) != RCMap.end()){
			RCMap.erase(0);
		}
	}
}

int IMGContainor::returnRCMapSize()
{
	int size = RCMap.size();
	return size;
}
