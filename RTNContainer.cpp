#include "RTNContainer.h"

string RTNContainer::returnName()
{
	return name;
}

ADDRINT RTNContainer::rturnAddress()
{
	return address;
}

bool RTNContainer::checkValid()
{
	return valid;
}

VOID RTNContainer::inputName(string newName)
{
	name = newName;
}

VOID RTNContainer::inputAddress(ADDRINT newAddress)
{
	address = newAddress;
}

VOID RTNContainer::changeValid()
{
	if (valid) {
		valid = false;
	}
	else {
		valid = true;
	}
}
