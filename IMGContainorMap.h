#pragma once
#include "pin.H"
#include <iostream>
#include <map>
#include <list>
#include "RTNContainer.h"
#include "IMGContainor.h"

class IMGContainorMap {

public:
	IMGContainorMap() {
		ADDRINT basic = 0;
		IMGContainor basicIMGC = IMGContainor();
		IMGCMap[basic] = basicIMGC;
	}

	VOID inputIMG(IMG inputIMG);
	
	VOID inputRTN(RTN inputRTN);
	
	bool checkContainIMG(IMG checkIMG);
	
	VOID trim();
	
	map<ADDRINT, IMGContainor> returnIMGCMap();

	int returnIMGNumber();

	list<string> returnIMGNames();


private:
	map<ADDRINT, IMGContainor> IMGCMap;
};