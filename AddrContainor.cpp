#include "AddrContainor.h"

VOID AddrContainor::inputCurrAddr(ADDRINT newAddr)
{
	insMap[newAddr] = prevAddr;
	prevAddr = newAddr;
	auto Tid = PIN_ThreadId();
	threadMap[newAddr] = Tid;
	auto Pid = PIN_GetPid();
	pidMap[newAddr] = Pid;
}

ADDRINT AddrContainor::returnPrevAddr(ADDRINT inputAddr)
{
	ADDRINT prev = insMap[inputAddr];
	return prev;
}

THREADID AddrContainor::returnTid(ADDRINT newAddr)
{
	return threadMap[newAddr];
}

INT AddrContainor::returnPid(ADDRINT newAddr)
{
	return pidMap[newAddr];
}
