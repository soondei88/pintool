#pragma once
#include "pin.H"
#include <iostream>
#include <string>

class RTNContainer {

public:
	RTNContainer()
		: name("name"), address(0), valid(false)
	{}

	RTNContainer(ADDRINT inputAddress)
	{
		RTN newRTN = RTN_FindByAddress(inputAddress);
		bool checkValid = RTN_Valid(newRTN);
		string newName = "";
		ADDRINT newAddress = 0;
		if (checkValid) {
			newName = RTN_Name(newRTN);
			newAddress = RTN_Address(newRTN);
		}
		else {
			newName = "name";
			newAddress = 0;
		}
		name = newName;
		address = newAddress;
		valid = checkValid;
	}

	string returnName();
	ADDRINT rturnAddress();
	bool checkValid();
	VOID inputName(string newName);
	VOID inputAddress(ADDRINT newAddress);
	VOID changeValid();


private:
	string name;
	ADDRINT address;
	bool valid;
};