#pragma once
#include "pin.H"
#include <map>
#include <iostream>

class AddrContainor {
public:
	AddrContainor()
		: prevAddr(0)
	{}

	VOID inputCurrAddr(ADDRINT newAddr);

	ADDRINT returnPrevAddr(ADDRINT inputAddr);

	THREADID returnTid(ADDRINT newAddr);

	INT returnPid(ADDRINT newAddr);

private:
	map<ADDRINT, ADDRINT> insMap;
	map<ADDRINT, THREADID> threadMap;
	map<ADDRINT, INT> pidMap;
	ADDRINT prevAddr; 

};