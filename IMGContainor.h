#pragma once
#include "pin.H"
#include "RTNContainer.h"
#include <iostream>
#include <map>

class IMGContainor {

public:
	IMGContainor() {

		RTNContainer basicRC = RTNContainer();
		ADDRINT basic = 0;
		RCMap[basic] = basicRC;
		address = 0;
		name = "name";
		valid = false;
	}

	IMGContainor(IMG inputImage) {

		bool inputImageValid = IMG_Valid(inputImage);
		if (inputImageValid) {
			string inputImageName = IMG_Name(inputImage);
			ADDRINT inputImageAddr = IMG_LowAddress(inputImage);
			address = inputImageAddr;
			name = inputImageName;
			valid = true;
			trim();
		}
		else {
			IMGContainor();
		}
	}

	bool checkValid();

	VOID changeValid();

	VOID inputName(string newName);

	VOID inputAddress(ADDRINT newAddress);

	ADDRINT returnAddress();
	
	string returnName();
	
	bool checkContainRTN(RTN checkRTN);
	
	VOID inputRTN(RTN inputRTN);
	
	VOID trim();
	
	int returnRCMapSize();

private:
	map<ADDRINT, RTNContainer> RCMap;
	ADDRINT address;
	string name;
	bool valid;
};